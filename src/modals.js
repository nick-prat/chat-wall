import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import React, {useState} from 'react';

// username validation regex
const usernameRegex = /^[a-zA-Z0-9]*$/;
const emailRegex = /^[A-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[A-Z0-9.-]+$/i;

import * as api from './api';

export const LoginModal = ({show, onClose, onLoggedIn}) => {
  const [errors, setErrors] = useState({});

  const validateForm = (username, password) => {
    const newErrors = {};

    if ( ! username || username === "" )
      newErrors.username = "Username can't be empty";
    else if ( username.length > 15 )
      newErrors.username = "Username can't be more then 15 characters";

    if ( ! password || password === "" )
      newErrors.password = "Password can't be empty";

    return newErrors;
  };

  const onSubmit = (e) => {
    e.preventDefault();

    const formData = new FormData(e.target);
    const {username, password} = Object.fromEntries(formData.entries());

    const newErrors = validateForm(username, password);

    if ( Object.keys(newErrors).length > 0 ) {
      setErrors(newErrors);
      return;
    }

    api.login(username, password).then((_) => {
      onLoggedIn();
      onClose();
      e.target.reset();
      setErrors({});
    }).catch((_) => {
      newErrors.username = "Invalid username/password";
      setErrors(newErrors);
    });
  };

  return (
    <Modal show={show} onHide={onClose}>
      <Modal.Header closeButton>
        <Modal.Title>Login</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={onSubmit}>
          <Form.Group className="mb-3" controlId="formBasicUsername">
            <Form.Label>Username</Form.Label>
            <Form.Control
              type="text"
              name="username"
              placeholder="Enter username"
              isInvalid={!!errors.username}
              />

            <Form.Control.Feedback type='invalid'>
              {errors.username}
            </Form.Control.Feedback>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              name="password"
              placeholder="Password"
              isInvalid={!!errors.password}
              />
            <Form.Control.Feedback type='invalid'>
              {errors.password}
            </Form.Control.Feedback>
          </Form.Group>

          <Button variant="primary" type="submit">
            Login
          </Button>
        </Form>
      </Modal.Body>
    </Modal>
  );
};

export const PasswordResetModal = ({show, onClose}) => {
  const [errors, setErrors] = useState({});

  const validateForm = (opassword, password1, password2) => {
    const newErrors = {};

    if ( ! opassword || opassword === "" )
      newErrors.opassword = "Password can't be empty";

    if ( ! password1 || password1 === "" )
      newErrors.password1 = "Password can't be empty";
    
    if ( ! password2 || password2 == "" )
      newErrors.password2 = "Password can't be empty";
    else if ( ! password2 || password2 !== password1 )
      newErrors.password2 = "Password doesn't match";

    return newErrors;
  };

  const onSubmit = (e) => {
    e.preventDefault();

    const formData = new FormData(e.target);
    const {opassword, password1, password2} = Object.fromEntries(formData.entries());

    const newErrors = validateForm(opassword, password1, password2);

    if ( Object.keys(newErrors).length > 0 ) {
      setErrors(newErrors);
      return;
    }

    api.resetPassword(opassword, password1).then((_) => {
      onClose();
      e.target.reset();
      setErrors({});
    }).catch((_) => {
      newErrors.opassword = "Invalid password";
      setErrors(newErrors);
    });
  };

  return (
    <Modal show={show} onHide={onClose}>
      <Modal.Header closeButton>
        <Modal.Title>Change Password</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={onSubmit}>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Old Password</Form.Label>
            <Form.Control
              type="password"
              name="opassword"
              placeholder="Password"
              isInvalid={!!errors.opassword}
              />
            <Form.Control.Feedback type='invalid'>
              {errors.opassword}
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>New Password</Form.Label>
            <Form.Control
              type="password"
              name="password1"
              placeholder="Password"
              isInvalid={!!errors.password1}
              />
            <Form.Control.Feedback type='invalid'>
              {errors.password1}
            </Form.Control.Feedback>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Retype New Password</Form.Label>
            <Form.Control
              type="password"
              name="password2"
              placeholder="Password"
              isInvalid={!!errors.password2}
              />
            <Form.Control.Feedback type='invalid'>
              {errors.password2}
            </Form.Control.Feedback>
          </Form.Group>

          <Button variant="primary" type="submit">
            Change Password
          </Button>
        </Form>
      </Modal.Body>
    </Modal>
  );
};

export const OTPasswordResetModal = ({onClose, token}) => {
  const [errors, setErrors] = useState({});

  const validateForm = (password1, password2) => {
    const newErrors = {};

    if ( ! password1 || password1 === "" )
      newErrors.password1 = "Password can't be empty";
    
    if ( ! password2 || password2 == "" )
      newErrors.password2 = "Password can't be empty";
    else if ( ! password2 || password2 !== password1 )
      newErrors.password2 = "Password doesn't match";

    return newErrors;
  };

  const onSubmit = (e) => {
    e.preventDefault();

    const formData = new FormData(e.target);
    const {password1, password2} = Object.fromEntries(formData.entries());

    const newErrors = validateForm(password1, password2);

    if ( Object.keys(newErrors).length > 0 ) {
      setErrors(newErrors);
      return;
    }

    api.oneTimeResetPassword(password1, token).then((_) => {
      onClose();
      e.target.reset();
      setErrors({});
    }).catch((_) => {
      newErrors.password1 = "Invalid password";
      setErrors(newErrors);
    });
  };

  return (
    <Modal show={true} onHide={onClose}>
      <Modal.Header closeButton>
        <Modal.Title>Change Password</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={onSubmit}>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>New Password</Form.Label>
            <Form.Control
              type="password"
              name="password1"
              placeholder="Password"
              isInvalid={!!errors.password1}
              />
            <Form.Control.Feedback type='invalid'>
              {errors.password1}
            </Form.Control.Feedback>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Retype New Password</Form.Label>
            <Form.Control
              type="password"
              name="password2"
              placeholder="Password"
              isInvalid={!!errors.password2}
              />
            <Form.Control.Feedback type='invalid'>
              {errors.password2}
            </Form.Control.Feedback>
          </Form.Group>

          <Button variant="primary" type="submit">
            Change Password
          </Button>
        </Form>
      </Modal.Body>
    </Modal>
  );
};

export const RequestPasswordResetModal = ({show, onClose}) => {
  const [errors, setErrors] = useState({});

  const validateForm = (username) => {
    const newErrors = {};

    if ( ! username || username === "" )
      newErrors.username = "Username can't be empty";
    else if ( username.length > 15 )
      newErrors.username = "Username can't be more then 15 characters";

    return newErrors;
  };

  const onSubmit = (e) => {
    e.preventDefault();

    const formData = new FormData(e.target);
    const {username} = Object.fromEntries(formData.entries());

    const newErrors = validateForm(username);

    if ( Object.keys(newErrors).length > 0 ) {
      setErrors(newErrors);
      return;
    }

    api.requestResetPassword(username).then((_) => {
      onClose();
      e.target.reset();
      setErrors({});
    }).catch((_) => {
      newErrors.username = "Invalid Username";
      setErrors(newErrors);
    });
  };

  return (
    <Modal show={show} onHide={onClose}>
      <Modal.Header closeButton>
        <Modal.Title>Reset Password</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={onSubmit}>
          <Form.Group className="mb-3" controlId="formBasicUsername">
            <Form.Label>Username</Form.Label>
            <Form.Control
              type="text"
              name="username"
              placeholder="Enter username"
              isInvalid={!!errors.username}
              />

            <Form.Control.Feedback type='invalid'>
              {errors.username}
            </Form.Control.Feedback>
          </Form.Group>

          <Button variant="primary" type="submit">
            Reset Password
          </Button>
        </Form>
      </Modal.Body>
    </Modal>
  );
};

export const RegisterModal = ({show, onClose, onLoggedIn}) => {
  const [errors, setErrors] = useState({});

  const validateForm = (username, email, password1, password2) => {
    const newErrors = {};

    if ( ! username || username === "" )
      newErrors.username = "Username can't be empty";
    else if ( username.length > 15 )
      newErrors.username = "Username can't be more then 15 characters";
    else if ( ! usernameRegex.test(username) )
      newErrors.username = "Username uses invalid characters";

    if ( ! email || email === "" )
      newErrors.email = "email can't be empty";
    else if ( ! emailRegex.test(email) )
      newErrors.email = "email uses invalid characters";

    if ( ! password1 || password1 === "" )
      newErrors.password1 = "Password can't be empty";
    
    if ( ! password2 || password2 == "" )
      newErrors.password2 = "Password can't be empty";
    else if ( ! password2 || password2 !== password1 )
      newErrors.password2 = "Password doesn't match";
    
      return newErrors;
  };

  const onSubmit = (e) => {
    e.preventDefault();

    const formData = new FormData(e.target);
    const {username, email, password1, password2} = Object.fromEntries(formData.entries());

    const newErrors = validateForm(username, email, password1, password2);

    if ( Object.keys(newErrors).length > 0 ) {
      setErrors(newErrors);
      return;
    }

    api.register(username, email, password1).then((_) => api.login(username, password1)).then((resp) => {
      onLoggedIn();
      onClose();
      e.target.reset();
      setErrors({});
    }).catch((err) => {
      newErrors.username = "Username taken";
      setErrors(newErrors);
    });
  };

  return (
    <Modal show={show} onHide={onClose}>
      <Modal.Header closeButton>
        <Modal.Title>Register</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={onSubmit}>
          <Form.Group className="mb-3" controlId="formBasicUsername">
            <Form.Label>Username</Form.Label>
            <Form.Control
              type="text"
              name="username"
              placeholder="Enter username"
              isInvalid={!!errors.username}
              />
            <Form.Control.Feedback type='invalid'>
              {errors.username}
            </Form.Control.Feedback>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email</Form.Label>
            <Form.Control
              type="text"
              name="email"
              placeholder="Enter email"
              isInvalid={!!errors.email}
              />
            <Form.Control.Feedback type='invalid'>
              {errors.email}
            </Form.Control.Feedback>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              name="password1"
              placeholder="Password"
              isInvalid={!!errors.password1}
              />
            <Form.Control.Feedback type='invalid'>
              {errors.password1}
            </Form.Control.Feedback>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Retype Password</Form.Label>
            <Form.Control
              type="password"
              name="password2"
              placeholder="Password"
              isInvalid={!!errors.password2}
              />
            <Form.Control.Feedback type='invalid'>
              {errors.password2}
            </Form.Control.Feedback>
          </Form.Group>

          <Button variant="primary" type="submit">
            Register
          </Button>
        </Form>
      </Modal.Body>
    </Modal>
  );
};