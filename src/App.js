
import './App.css';
import React, {useEffect, useState} from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  useParams,
  useNavigate
} from "react-router-dom";

import * as api from './api';
import * as components from './components.js';
import * as modals from './modals';

// Consumes a token passed in as a URL param, used for password reset
const ResetPassword = () => {
  const navigate = useNavigate();
  const {token} = useParams();

  useEffect(() => api.consumeToken(token).then((_) => navigate('/')), []);

  return (
    <modals.OTPasswordResetModal
      onClose={() => navigate('/')}
      token={token}
      />
  );
};

const App = () => {
  const [showLoginModal, setShowLoginModal] = useState(false);
  const [showRegisterModal, setShowRegisterModal] = useState(false);
  const [showRequestPasswordResetModal, setShowRequestPasswordResetModal] = useState(false);
  const [showPasswordResetModal, setShowPasswordResetModal] = useState(false);
  const [loggedIn, setLoggedIn] = useState(false);
  const [user, setUser] = useState(null);

  const refreshUser = () => {
    api.getCurrentUser().then((resp) => {
      setLoggedIn(true);
      setUser(resp.data);
      console.log(user);
    }).catch((_) => {/* ignored */});
  };

  const onLoggedIn = () => {
    setLoggedIn(true);
    refreshUser();
  };

  const onLoggedOut = () => {
    setLoggedIn(false);
    setUser(null);
  };

  useEffect(() => refreshUser(), []);

  return (
    <div className="App">
      {/* <header className="App-header"> */}
        <modals.LoginModal
          show={showLoginModal}
          onClose={() => setShowLoginModal(false)}
          onLoggedIn={onLoggedIn}
          />
        <modals.RegisterModal
          show={showRegisterModal}
          onClose={() => setShowRegisterModal(false)}
          onLoggedIn={onLoggedIn}
          />
        <modals.RequestPasswordResetModal
          show={showRequestPasswordResetModal}
          onClose={() => setShowRequestPasswordResetModal(false)}
          />
        <modals.PasswordResetModal
          show={showPasswordResetModal}
          onClose={() => setShowPasswordResetModal(false)}
          />
        <components.ChatWall
          // Passing all these callbacks seems wrong
          onShowLogin={() => setShowLoginModal(true)}
          onShowRegister={() => setShowRegisterModal(true)}
          onShowRequestPasswordReset={() => setShowRequestPasswordResetModal(true)}
          onShowPasswordReset={() => setShowPasswordResetModal(true)}
          showLoginButton={!loggedIn}
          onLoggedOut={onLoggedOut}
          user={user}
          />
      {/* </header> */}
    </div>
  );
};

const AppRouter = () => (
  <Router>
    <Routes>
      <Route path='/' element={<App/>}/>
      <Route path='/user/:uid' element={<App/>}/>
      <Route path='/reset-password/:token' element={<ResetPassword/>}/>
    </Routes>
  </Router>
);

export default AppRouter;
