import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import React, {useEffect, useState} from 'react';

import * as api from './api';
import { useNavigate, useParams } from 'react-router';
// import { useIsFocused } from "@react-navigation/native";

// Shows a single post from a user
export const Post = ({user, post, onRefreshNeeded}) => {
  const navigate = useNavigate();

  return (
    <div className="post-box">
      <div className="post-text-header">
        <div>
          <button className="title-text" onClick={() => navigate(`/user/${post.cid}`)}>
            {`${post.creator} - ${new Date(post.date).toLocaleString()}`}
          </button>
        </div>
        {
          (user && user.id == post.cid)
            ? <button onClick={() => api.deleteTextPost(post.id).then((_) => onRefreshNeeded())}>&times;</button> 
            : <div/>
        }
      </div>
      <hr/>
      <p>{post.text}</p>
    </div>
  );
};
  

// Shows a list of all the posts from all the users
export const PostList = ({posts, onRefreshNeeded, user}) => (
  <div className="post-container">
    {posts.map((post) => {
      return (
        <Post
          key={post.id}
          user={user}
          post={post}
          onRefreshNeeded={onRefreshNeeded}
          />
      );
    })}
  </div>
);

// Container that you can write a message in and submit
export const PostSubmission = ({onRefreshNeeded, loggedIn}) => {
  const [errors, setErrors] = useState({});

  const validateForm = (postText) => {
    const newErrors = {};

    if ( ! postText || postText === "" )
      newErrors.postText = "Post can't be empty";
    else if ( postText.length > 500 )
      newErrors.postText = "Post can't be more then 500 characters";

    return newErrors;
  };

  const onSubmit = (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const {postText} = Object.fromEntries(formData.entries());

    const newErrors = validateForm(postText);

    if ( Object.keys(newErrors).length > 0 ) {
      setErrors(newErrors);
      return;
    }

    console.log(postText);
    api.createPost(postText).then((_) => {
      e.target.reset();
      setErrors({});
      onRefreshNeeded();
    });
  };

  return (
    <div className="submission-container">
      <Form onSubmit={onSubmit}>
        <Form.Group className="submission-text-container" controlId="formBasicText">
          <Form.Control
            as="textarea"
            maxLength="500"
            rows="5"
            name="postText"
            placeholder="Post a Message!"
            autoComplete="off"
            isInvalid={!!errors.postText}
            />
          <Form.Control.Feedback type='invalid'>
            {errors.postText}
          </Form.Control.Feedback>
        </Form.Group>

        <div className="submission-button-container">
          <Button variant="primary" type="submit" disabled={!loggedIn}>
            Post
          </Button>
        </div>
      </Form>
    </div>
  );
};

export const Header = ({onShowLogin, onShowRegister, onShowRequestPasswordReset, onShowPasswordReset, showLoginButton, onLoggedOut, user, onRefreshNeeded}) => {
  const logout = () => {
    api.logout().then((_) => onLoggedOut());
  };

  return (
    <div className="chat-wall-header">
      <div className="chat-wall-header-content">
        {
          showLoginButton
            ? <div/>
            : <div className="chat-wall-header-user"><p className="title-text">{`Logged in as ${user && user.username}`}</p></div>
        }
        <div className="chat-wall-header-button-container">
          <Button variant="primary" onClick={onRefreshNeeded}>Refresh</Button>
          {
            showLoginButton 
              ? (
                  <div>
                    <Button variant="primary" onClick={onShowRequestPasswordReset}>Reset Password</Button>
                    <Button variant="primary" onClick={onShowLogin}>Login</Button>
                    <Button variant="primary" onClick={onShowRegister}>Register</Button>
                  </div>
                )
              : (
                  <div>
                    <Button variant="primary" onClick={onShowPasswordReset}>Change Password</Button>
                    <Button variant="primary" onClick={logout}>Logout</Button>
                  </div>
                )
          }
        </div>
      </div>
      <hr/>
    </div>
  );
};

export const ChatWall = ({
  onShowLogin, onShowRegister, onShowRequestPasswordReset, onShowPasswordReset,
  showLoginButton, onLoggedOut, user
}) => {
  const [posts, setPosts] = useState([]);
  const {uid} = useParams();
  // const isFocused = useIsFocused();

  const fetchPosts = () => {
    console.log("foo");
    (uid ? api.getUserTextPosts(uid) : api.getTextPosts())
      .then((resp) => {
        console.log(resp);
        setPosts(resp.data);
      })
      .catch((err) => {
        console.error(err);
      });
  };

  useEffect(() => fetchPosts(), [uid]);

  return (
    <div>
      <Header
        onShowLogin={onShowLogin}
        onShowRegister={onShowRegister}
        onShowRequestPasswordReset={onShowRequestPasswordReset}
        onShowPasswordReset={onShowPasswordReset}
        showLoginButton={showLoginButton}
        onLoggedOut={onLoggedOut}
        user={user}
        onRefreshNeeded={fetchPosts}
        />
      <div className="wall-container">
        <PostSubmission
          onRefreshNeeded={fetchPosts}
          loggedIn={user && user.id !== 0}
          />
        <PostList
          onRefreshNeeded={fetchPosts}
          posts={posts}
          user={user}
          />
      </div>
    </div>
  );
};