import axios from 'axios';

const instance = axios.create({
  baseURL: 'http://localhost:8000/api/',
  withCredentials: true
});

// Below are a list of available API requests on the chat-wall-api

// Text Post API calls
export const getTextPosts = () => {
  return instance.get('text_post/');
};

export const getTextPost = (id) => {
  return instance.get(`text_post/${id}/`);
};

export const deleteTextPost = (id) => {
  return instance.delete(`text_post/${id}/`);
};

export const createPost = (text) => {
  return instance({
    method: 'post',
    url: 'text_post/',
    data: {
      text: text
    }
  });
};

// User API calls
export const getUsers = () => {
  return instance.get('users/');
};

export const getUser = (id) => {
  return instance.get(`users/${id}/`);
};

export const getUserTextPosts = (id) => {
  return instance.get(`users/${id}/text_post/`);
};

// Used to check if a user is logged in as well getting current user info
export const getCurrentUser = () => {
  return instance.get('users/status/');
};

// Login/Logout/Register API calls
export const login = (username, password) => {
  return instance({
    method: 'post',
    url: 'users/login/',
    data: {
      username: username,
      password: password
    }
  });
};

export const logout = () => {
  return instance.post('users/logout/');
};

export const register = (username, email, password) => {
  return instance({
    method: 'post',
    url: 'users/',
    data: {
      username: username,
      password: password,
      email: email
    }
  });
};

// Password API calls
export const resetPassword = (oldPassword, newPassword) => {
  return instance({
    method: 'patch',
    url: 'users/password/reset/',
    data: {
      old_password: oldPassword,
      new_password: newPassword
    }
  });
};

export const oneTimeResetPassword = (newPassword, token) => {
  return instance({
    method: 'patch',
    url: 'users/password/otreset/',
    data: {
      password: newPassword
    },
    headers: {
      Authorization: `OTToken ${token}`
    }
  });
};

export const requestResetPassword = (username) => {
  return instance({
    method: 'post',
    url: 'users/password/request/ ',
    data: {
      username: username
    }
  });
};

export const consumeToken = (token) => {
  return instance({
    method: 'post',
    url: 'token/consume/',
    headers: {
      Authorization: `Token ${token}`
    }
  });
};
