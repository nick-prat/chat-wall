# Chat Wall Front End

Chat Wall app created using React.js + React Bootstrap + Django Rest Framework backend

## Setup & Running

1) npm install
2) npm start
3) go to localhost:3000

## Features

1) Password reset flow using email + one time login tokens
2) User registration/login + email notification
3) Create Text Post
4) Delete Text Post if you are the one that created them (an x will appear on your messages)
5) Filter messages by user by clicking on the author of the post

## Screenshot

![Home Page](/screenshot/screenshot.png)